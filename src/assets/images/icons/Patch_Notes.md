# "Patch" Notes

- added new icon for Cultural Blessings
- added new icon for Calling - I was in the mood - no hurry; for later :-)
- added new icon for Heroic Culture - I was in the mood - no hurry; for later :-)
- added new icon for ranged Adversary weapons
- added new icon for bestial Adversary weapons
- added new icon for bestial Adversary armour
- added new icon for Bows
- added new icon for Axes
- added new icon for Spears
- added new icon for Headgear
- addes new icon fpr Shields
- renamed "weapon.png" to "weapon_swords.png"
- renamed icon "adversary_weapon.png" to "adversary_weapon_close.png"