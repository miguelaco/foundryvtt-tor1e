export class TORBaseDie extends Die {
    constructor(termData) {
        termData.faces = 6;
        super(termData);
    }

    /* -------------------------------------------- */

    /** @override */
    static DENOMINATION = 's';

    static COMMAND = `d${TORBaseDie.DENOMINATION}`;

    /* -------------------------------------------- */
    /** @override */
    static getResultLabel(result) {
        return CONFIG.tor1e.STANDARD_RESULTS[result].label;
    }

    /**
     *
     * @param results
     * @returns {*} the value of the die
     */
    static getResultValue(results) {
        return results.map(die =>
            CONFIG.tor1e.STANDARD_RESULTS[die.result].result).reduce((a, b) => a + b, 0);
    }
}

export class TORWearyBaseDie extends Die {
    constructor(termData) {
        termData.faces = 6;
        super(termData);
    }

    /* -------------------------------------------- */

    /** @override */
    static DENOMINATION = 'w';

    static COMMAND = `d${TORWearyBaseDie.DENOMINATION}`;

    /* -------------------------------------------- */
    /** @override */
    static getResultLabel(result) {
        return CONFIG.tor1e.WEARY_RESULTS[result].label;
    }

    static getResultValue(results) {
        return results.map(die =>
            CONFIG.tor1e.WEARY_RESULTS[die.result].result).reduce((a, b) => a + b, 0);
    }
}

export class TORFeatBaseDie extends Die {
    constructor(termData) {
        termData.faces = 12;
        super(termData);
    }

    /* -------------------------------------------- */

    /** @override */
    static DENOMINATION = 'f';

    static COMMAND = `d${TORFeatBaseDie.DENOMINATION}`;

    /* -------------------------------------------- */
    /** @override */
    static getResultLabel(result) {
        return CONFIG.tor1e.FEAT_RESULTS[result].label;
    }

    static getResultValue(results) {
        return results.map(die =>
            CONFIG.tor1e.FEAT_RESULTS[die.result].result).reduce((a, b) => a + b, 0);
    }
}
