import {tor1e} from "./config.js";
import Tor1eItemSheet from "./sheets/items/Tor1eItemSheet.js";
import Tor1eNpcSheet from "./sheets/actors/Tor1eNpcSheet.js";
import Tor1eAdversarySheet from "./sheets/actors/Tor1eAdversarySheet.js";
import Tor1eCharacterSheet from "./sheets/actors/Tor1eCharacterSheet.js";
import {TORBaseDie, TORFeatBaseDie, TORWearyBaseDie} from "./die.js";
import Tor1eItem from "./Tor1eItem.js";
import {Tor1eRoll} from "./Tor1eRoll.js";


Hooks.once("init", async function () {
    console.log("TOR1E | Initializing The One Ring 1st edition system.");

    CONFIG.tor1e = tor1e;

    // Define custom Roll class
    CONFIG.Dice.rolls = [...CONFIG.Dice.rolls, Tor1eRoll];

    CONFIG.Item.entityClass = Tor1eItem;

    CONFIG.tor1e.STANDARD_RESULTS = {
        1: {label: `<img src="systems/tor1e/assets/images/dice/s_1.png" alt="{{localize tor1e.dice.standard.1}}" />`, result: 1},
        2: {label: `<img src='systems/tor1e/assets/images/dice/s_2.png'  alt="{{localize tor1e.dice.standard.2}}" />`, result: 2},
        3: {label: `<img src='systems/tor1e/assets/images/dice/s_3.png'  alt="{{localize tor1e.dice.standard.3}}" />`, result: 3},
        4: {label: `<img src='systems/tor1e/assets/images/dice/s_4.png'  alt="{{localize tor1e.dice.standard.4}}" />`, result: 4},
        5: {label: `<img src='systems/tor1e/assets/images/dice/s_5.png'  alt="{{localize tor1e.dice.standard.5}}" />`, result: 5},
        6: {label: `<img src='systems/tor1e/assets/images/dice/s_6.png'  alt="{{localize tor1e.dice.standard.6}}" />`, result: 6},
    };

    CONFIG.tor1e.WEARY_RESULTS = {
        1: {label: `<img src="systems/tor1e/assets/images/dice/s_1_w.png" alt="{{localize tor1e.dice.weary.1}}" />`, result: 0},
        2: {label: `<img src='systems/tor1e/assets/images/dice/s_2_w.png'  alt="{{localize tor1e.dice.weary.2}}" />`, result: 0},
        3: {label: `<img src='systems/tor1e/assets/images/dice/s_3_w.png'  alt="{{localize tor1e.dice.weary.3}}" />`, result: 0},
        4: {label: `<img src='systems/tor1e/assets/images/dice/s_4.png'  alt="{{localize tor1e.dice.weary.4}}" />`, result: 4},
        5: {label: `<img src='systems/tor1e/assets/images/dice/s_5.png'  alt="{{localize tor1e.dice.weary.5}}" />`, result: 5},
        6: {label: `<img src='systems/tor1e/assets/images/dice/s_6.png'  alt="{{localize tor1e.dice.weary.6}}" />`, result: 6},
    };

    CONFIG.tor1e.FEAT_RESULTS = {
        1: {label: `<img src="systems/tor1e/assets/images/dice/f_1.png" alt="{{localize tor1e.dice.feat.1}}" />`, result: 1},
        2: {label: `<img src='systems/tor1e/assets/images/dice/f_2.png'  alt="{{localize tor1e.dice.feat.2}}" />`, result: 2},
        3: {label: `<img src='systems/tor1e/assets/images/dice/f_3.png'  alt="{{localize tor1e.dice.feat.3}}" />`, result: 3},
        4: {label: `<img src='systems/tor1e/assets/images/dice/f_4.png'  alt="{{localize tor1e.dice.feat.4}}" />`, result: 4},
        5: {label: `<img src='systems/tor1e/assets/images/dice/f_5.png'  alt="{{localize tor1e.dice.feat.5}}" />`, result: 5},
        6: {label: `<img src='systems/tor1e/assets/images/dice/f_6.png'  alt="{{localize tor1e.dice.feat.6}}" />`, result: 6},
        7: {label: `<img src="systems/tor1e/assets/images/dice/f_7.png" alt="{{localize tor1e.dice.feat.7}}" />`, result: 7},
        8: {label: `<img src='systems/tor1e/assets/images/dice/f_8.png'  alt="{{localize tor1e.dice.feat.8}}" />`, result: 8},
        9: {label: `<img src='systems/tor1e/assets/images/dice/f_9.png'  alt="{{localize tor1e.dice.feat.9}}" />`, result: 9},
        10: {label: `<img src='systems/tor1e/assets/images/dice/f_10.png'  alt="{{localize tor1e.dice.feat.10}}" />`, result: 10},
        11: {label: `<img src='systems/tor1e/assets/images/dice/f_eye.png'  alt="{{localize tor1e.dice.feat.11}}" />`, result: 0},
        12: {label: `<img src='systems/tor1e/assets/images/dice/f_gandalf.png'  alt="{{localize tor1e.dice.feat.12}}" />`, result: 12},
    };

    Items.unregisterSheet("core", ItemSheet)
    Items.registerSheet("tor1e", Tor1eItemSheet, {makeDefault: true});

    Actors.unregisterSheet("core", ActorSheet)
    Actors.registerSheet("tor1e", Tor1eNpcSheet, {types: ["npc"], makeDefault: true});
    Actors.registerSheet("tor1e", Tor1eAdversarySheet, {types: ["adversary"], makeDefault: true});
    Actors.registerSheet("tor1e", Tor1eCharacterSheet, {types: ["character"], makeDefault: true});

    preloadHandlebarsTemplates();
})

async function preloadHandlebarsTemplates() {
    const templatePaths = [
        // Common
        "systems/tor1e/templates/sheets/actors/partials/common/skill-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/common/weapon-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/common/simple-talent-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/common/actor-header-card.hbs",

        // Aversary
        "systems/tor1e/templates/sheets/actors/partials/adversary/special-ability-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/adversary/adversary-skill-card.hbs",

        // Character
        "systems/tor1e/templates/sheets/actors/partials/character/character-biography-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-attribute-sidebar-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-attributes-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-common-skills-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-skill-groups-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-weapon-skills-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-combat-attributes-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-armour-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-weapon-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-state-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-state-of-health-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-combat-attributes-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-resources-card.hbs",

        // Messages
        "systems/tor1e/templates/sheets/messages/partials/common/skill-roll-card.hbs",
        "systems/tor1e/templates/sheets/messages/partials/common/tooltip-tor1e.hbs",

        // Components
        "systems/tor1e/templates/sheets/actors/components/stat-square-card.hbs",
        "systems/tor1e/templates/sheets/actors/components/stat-circle-card.hbs",
        "systems/tor1e/templates/sheets/actors/components/complex-stat-circle-card.hbs",
        "systems/tor1e/templates/sheets/actors/components/complex-stat-square-card.hbs",
        "systems/tor1e/templates/sheets/actors/components/complex-item-square-card.hbs",
        "systems/tor1e/templates/sheets/actors/components/complex-item-circle-card.hbs",
        "systems/tor1e/templates/sheets/actors/components/skill-card.hbs",
    ];

    CONFIG.Dice.terms['s'] = TORBaseDie;
    CONFIG.Dice.terms['w'] = TORWearyBaseDie;
    CONFIG.Dice.terms['f'] = TORFeatBaseDie;

    return loadTemplates(templatePaths);
}

Hooks.once('diceSoNiceReady', (dice3d) => {
    dice3d.addSystem({id: "tor1e", name: "The One Ring"}, true);

    //tor1e dice
    dice3d.addDicePreset({
        type: "ds",
        labels: ["systems/tor1e/assets/images/dice/s_1.png",
            "systems/tor1e/assets/images/dice/s_2.png",
            "systems/tor1e/assets/images/dice/s_3.png",
            "systems/tor1e/assets/images/dice/s_4.png",
            "systems/tor1e/assets/images/dice/s_5.png",
            "systems/tor1e/assets/images/dice/s_6.png"],
        system: "tor1e"
    }, "d6");

    dice3d.addDicePreset({
        type: "dw",
        labels: ["systems/tor1e/assets/images/dice/s_1_w.png",
            "systems/tor1e/assets/images/dice/s_2_w.png",
            "systems/tor1e/assets/images/dice/s_3_w.png",
            "systems/tor1e/assets/images/dice/s_4.png",
            "systems/tor1e/assets/images/dice/s_5.png",
            "systems/tor1e/assets/images/dice/s_6.png"],
        system: "tor1e"
    }, "d6");

    dice3d.addDicePreset({
        type: "df",
        labels: ["systems/tor1e/assets/images/dice/f_1.png",
            "systems/tor1e/assets/images/dice/f_2.png",
            "systems/tor1e/assets/images/dice/f_3.png",
            "systems/tor1e/assets/images/dice/f_4.png",
            "systems/tor1e/assets/images/dice/f_5.png",
            "systems/tor1e/assets/images/dice/f_6.png",
            "systems/tor1e/assets/images/dice/f_7.png",
            "systems/tor1e/assets/images/dice/f_8.png",
            "systems/tor1e/assets/images/dice/f_9.png",
            "systems/tor1e/assets/images/dice/f_10.png",
            "systems/tor1e/assets/images/dice/f_eye.png",
            "systems/tor1e/assets/images/dice/f_gandalf.png"],
        system: "tor1e"
    }, "d12");

});
