import * as Dice from "./sheets/dice.js";

export const tor1eUtilities = {};

/**
 * Take an array of values and split it in 2 part depending on the index in the array
 * @param abilities
 * @returns {*[]}
 * @private
 */
function _splitInTwo(abilities) {
    let lefts = abilities.filter((v, i) => !(i % 2));
    let rights = abilities.filter((v, i) => i % 2);
    return [lefts, rights]
}


tor1eUtilities.filtering = {
    /**
     * Filter a list of items by its type (weapons, specialAbility, armour, ...) and its group ( for specialAbility for example, it could be Hate, Virtues, Valor)
     * This method returns left and right because it is used in a Two columns display.
     * @param items
     * @param type
     * @param subgroup
     * @returns {*[]}
     */
    "getAndSplitItemsBy": function (items, type, subgroup) {
        let elements = this.getItemsBy(items, type, subgroup);
        return _splitInTwo(elements);
    },
    /**
     * Get a list of elements from Items filtered by the Type (Armour, weapon, special ability, ...)
     * @param items List of all items
     * @param type The type of items you want to filter
     * @param subgroup The subgroup of type of Item you want to filter (optional)
     * @returns {*}
     */
    "getItemsBy": function (items, type, subgroup = null) {
        if (subgroup == null) {
            return items.filter(function (item) {
                return item.type === type;
            });
        } else {
            return items.filter(function (item) {
                return item.type === type && item.data.group.value === subgroup;
            });
        }
    },
    /**
     * Get a list of elements from Items filtered by the Type (Armour, weapon, special ability, ...)
     * @param items List of all items
     * @param type The type of items you want to filter
     * @param subgroup The subgroup of type of Item you want to filter (optional)
     * @returns {*}
     */
    "getItemsNot": function (items, type, subgroup) {
        return items.filter(function (item) {
            return item.type === type && item.data.group.value !== subgroup;
        });
    },
    /**
     * Get a list of elements from Items filtered by the Type (Armour, weapon, special ability, ...)
     * @param items List of all items
     * @param type The type of items you want to filter
     * @param subgroup The subgroup of type of Item you want to filter (optional)
     * @returns {*}
     */
    "getItemBy": function (items, type, subgroup = null) {
        if (subgroup == null) {
            return items.find(function (item) {
                return item.type === type;
            });
        } else {
            return items.find(function (item) {
                return item.type === type && item.data.group.value === subgroup;
            });
        }
    },
    /**
     * Get a list of elements from Items filtered by the Type (Armour, weapon, special ability, ...)
     * The list of elements can be filtered by a list of groups. All the elements belonging to one of the groups.
     * @param items
     * @param type
     * @param groups
     * @returns {*}
     */
    "getItemIn": function (items, type, groups = null) {
        return items.find(function (item) {
            if (groups === null) {
                return item.type === type;
            } else {
                return item.type === type && groups.includes(item.data.group.value)
            }
        });
    }
}

/**
 * Get itemId from an event by bubbling to the item class and get the _id
 * @param event
 * @returns {string}
 * @private
 */
function _getItemId(event) {
    let element = event.currentTarget;
    return element.closest(".item").dataset.itemId;
}

tor1eUtilities.eventsProcessing = {
    /**
     * Roll some dice if you click on stats depending on the value of the stat
     * @param event
     */
    "onStatRoll": function (event) {
        let element = event.currentTarget;
        let favouredBonus = (element.dataset.actionfavoured === "true") ? this.actor.data.data.attributeLevel.value : 0;
        let actionBonusRaw = element.dataset.actionValueBonus;
        let actionBonus = actionBonusRaw === "" ? 0 : parseInt(actionBonusRaw);
        let actionDiceRaw = element.dataset.actionValueDice;
        let actionDice = actionDiceRaw === "" ? 0 : parseInt(actionDiceRaw);

        Dice.taskCheck({
            actor: this.actor,
            user: game.user,
            actionValue: actionDice,
            actionName: element.dataset.actionName,
            wearyRoll: this.actor.data.data.stateOfHealth.weary.value,
            modifier: favouredBonus + actionBonus
        });
    },
    /**
     * Roll some dice if you click on skills depending on the value of the skill
     * @param event
     */
    "onSkillRoll": function (event) {
        let element = event.currentTarget;

        Dice.taskCheck({
            actor: this.actor,
            user: game.user,
            actionValue: element.dataset.actionValue,
            actionName: element.dataset.actionName,
            wearyRoll: this.actor.data.data.stateOfHealth.weary.value,
            modifier: (element.dataset.actionFavoured === "true") ? this.actor.data.data.attributeLevel.value : 0
        });
    },
    /**
     * Roll some dice if you click on anitem depending on the value of the item stat
     * @param event
     */
    "onItemRoll": function (event) {
        let element = event.currentTarget;

        Dice.taskCheck({
            actor: this.actor,
            user: game.user,
            actionValue: element.dataset.actionValue,
            actionName: element.dataset.actionName,
            wearyRoll: this.actor.data.data.stateOfHealth.weary.value,
            modifier: (element.dataset.actionFavoured === "true") ? this.actor.data.data.attributeLevel.value : 0
        });
    },

    /**
     * Delete an item when you click on the trash
     * @param event
     * @returns {*}
     */
    "onItemDelete": function (event) {
        event.preventDefault();
        let itemId = _getItemId(event);

        return this.actor.deleteOwnedItem(itemId);
    },

    /**
     * Update the value of a stat when you change the value in the input text box.
     * Should be used for example when an item is in a sheet because the object doesn't belong to the proper stat of the sheet.
     * @param event
     * @returns {*}
     */
    "onSkillEdit": function (event) {
        event.preventDefault();
        let element = event.currentTarget;
        let itemId = _getItemId(event);
        let item = this.actor.getOwnedItem(itemId);
        let field = element.dataset.field;

        // if the element is a checkbox then we have to translate the state (chack) into a boolean
        if (element.type === "checkbox") {
            return item.update({[field]: element.checked === true});
        }
        return item.update({[field]: element.value});
    },
    /**
     * Display a message about the Item in the chat
     * @param event
     */
    "onItemChat": function (event) {
        event.preventDefault();
        let itemId = _getItemId(event);
        let item = this.actor.getOwnedItem(itemId);

        item.roll();
    }

}