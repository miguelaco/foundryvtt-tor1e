import {tor1eUtilities} from "../../utilities.js";

export default class Tor1eNpcSheet extends ActorSheet {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["tor1e", "sheet", "actor"],
            width: 500,
            height: 725,
            template: `${CONFIG.tor1e.properties.rootpath}/templates/sheets/actors/nonplayercharacter-sheet.hbs`
        });
    }

    getData() {
        const data = super.getData();
        let constants = CONFIG.tor1e.constants;
        data.config = CONFIG.tor1e;

        data.specialities = tor1eUtilities.filtering.getItemsBy(data.items, constants.trait, constants.speciality);
        data.distinctiveFeatures = tor1eUtilities.filtering.getItemsBy(data.items, constants.trait, constants.distinctiveFeature);
        data.shadowWeaknesses = tor1eUtilities.filtering.getItemsBy(data.items, constants.trait, constants.shadowWeakness);
        data.skills = tor1eUtilities.filtering.getItemsNot(data.items, constants.skill, constants.combat);
        data.weaponSkills = tor1eUtilities.filtering.getItemsBy(data.items, constants.skill, constants.combat);

        return data;
    }

    activateListeners(html) {
        /*
            code pattern
            html.find(cssSelector).event(this._someCallBack.bind(this));
         */
        html.find(".item-delete").click(tor1eUtilities.eventsProcessing.onItemDelete.bind(this));
        html.find(".inline-edit").change(tor1eUtilities.eventsProcessing.onSkillEdit.bind(this));

        // Owner-only listeners
        if (this.actor.owner) {
            html.find(".item-chat").click(tor1eUtilities.eventsProcessing.onItemChat.bind(this));
            html.find(".item-roll").click(tor1eUtilities.eventsProcessing.onItemRoll.bind(this));
        }

        super.activateListeners(html);
    }

}