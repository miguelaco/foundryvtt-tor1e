import {tor1eUtilities} from "../../utilities.js";

export default class Tor1eAdversarySheet extends ActorSheet {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["tor1e", "sheet", "actor"],
            width: 550,
            height: 775,
            template: `${CONFIG.tor1e.properties.rootpath}/templates/sheets/actors/adversary-sheet.hbs`
        });
    }

    getData() {
        const data = super.getData();
        let constants = CONFIG.tor1e.constants;
        data.config = CONFIG.tor1e;

        [data.leftSpecialAbilities, data.rightSpecialAbilities] =
            tor1eUtilities.filtering.getAndSplitItemsBy(data.items, constants.specialAbility, constants.hate);

        data.weapons = tor1eUtilities.filtering.getItemsBy(data.items, constants.weapon);
        data.headgear = tor1eUtilities.filtering.getItemBy(data.items, constants.armour, constants.headgear);
        data.shield = tor1eUtilities.filtering.getItemBy(data.items, constants.armour, constants.shield);
        data.armour = tor1eUtilities.filtering.getItemIn(data.items, constants.armour, [constants.mailArmour, constants.leatherArmour]);

        return data;
    }

    activateListeners(html) {
        /*
            code pattern
            html.find(cssSelector).event(this._someCallBack.bind(this));
         */
        html.find(".item-delete").click(tor1eUtilities.eventsProcessing.onItemDelete.bind(this));
        html.find(".inline-edit").change(tor1eUtilities.eventsProcessing.onSkillEdit.bind(this));

        // Owner-only listeners
        if (this.actor.owner) {
            html.find(".item-roll").click(tor1eUtilities.eventsProcessing.onItemRoll.bind(this));
            html.find(".skill-roll").click(tor1eUtilities.eventsProcessing.onSkillRoll.bind(this));
            html.find(".stat-roll").click(tor1eUtilities.eventsProcessing.onStatRoll.bind(this));
        }
        super.activateListeners(html);
    }

}