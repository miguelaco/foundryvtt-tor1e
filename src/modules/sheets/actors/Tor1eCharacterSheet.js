export default class Tor1eCharacterSheet extends ActorSheet {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["tor1e", "sheet", "actor"],
            width: 600,
            height: 800,
            template: `${CONFIG.tor1e.properties.rootpath}/templates/sheets/actors/character-sheet.hbs`
        });
    }

    getData() {
        const data = super.getData();
        let constants = CONFIG.tor1e.constants;
        data.config = CONFIG.tor1e;

        function getItemBy(group) {
            return data.items.filter(function (item) {
                return item.type === group;
            });
        }

        function getTraitBy(group) {
            return data.items.filter(function (item) {
                return item.type === constants.trait && item.data.group.value === group;
            });
        }

        data.specialities = getTraitBy(constants.speciality);
        data.distinctiveFeatures = getTraitBy(constants.distinctiveFeature);

        data.weaponSkills = data.items
            .filter(function (item) {
                return item.type === constants.skill && item.data.group.value === constants.combat
            });

        data.rewards = getItemBy(constants.reward);
        data.virtues = getItemBy(constants.virtues);
        data.armours = getItemBy(constants.armour);
        data.weapons = getItemBy(constants.weapon);

        return data;
    }

    activateListeners(html) {
        /*
            code pattern
            html.find(cssSelector).event(this._someCallBack.bind(this));
         */
        html.find(".item-delete").click(this._onItemDelete.bind(this));
        html.find(".inline-edit").change(this._onSkillEdit.bind(this));

        super.activateListeners(html);
    }

    _onItemDelete(event) {
        event.preventDefault();
        let element = event.currentTarget;
        let itemId = element.closest(".item").dataset.itemId;

        return this.actor.deleteOwnedItem(itemId);
    }

    _onSkillEdit(event) {
        event.preventDefault();
        let element = event.currentTarget;

        let itemId = element.closest(".item").dataset.itemId;
        let item = this.actor.getOwnedItem(itemId);
        let field = element.dataset.field;

        // if the element is a checkbox then we have to translate the state (chack) into a boolean
        if (element.type === "checkbox") {
            return item.update({[field]: element.checked === true});
        }
        return item.update({[field]: element.value});
    }

}