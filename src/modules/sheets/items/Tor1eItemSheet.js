export default class Tor1eItemSheet extends ItemSheet {

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["tor1e", "sheet", "item"],
            width: 400,
            height: 500
        });
    }

    get template() {
        return `systems/tor1e/templates/sheets/items/${this.item.data.type}-sheet.hbs`
    }

    getData() {
        const data = super.getData();
        data.config = CONFIG.tor1e;
        return data;
    }
}