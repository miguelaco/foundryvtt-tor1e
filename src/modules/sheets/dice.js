import * as Tor1eDie from "../die.js"
import {Tor1eRoll} from "../Tor1eRoll.js";

export async function taskCheck(
    {
        actor = null,
        user = null,
        actionValue = null,
        actionName = "",
        difficulty = 14,
        wearyRoll = false,
        modifier = 0
    } = {}) {

    Object.defineProperty(String.prototype, "sanityze", {
        value: function sanityze() {
            return this.replace(/^\r?\n|\r/, "");
        },
        writable: true,
        configurable: true
    });

    function _buildRollFormula() {
        let bonus = (modifier !== 0) ? ` + ${modifier}` : ""
        let baseDice = wearyRoll === true ? wearyDie : standardDie;
        let successDice = actionValue > 0 ? ` + (@actionValue)${baseDice} ` : ""
        return `1${featDie}${successDice}${bonus}`;
    }

    let standardDie = Tor1eDie.TORBaseDie.COMMAND;
    let wearyDie = Tor1eDie.TORWearyBaseDie.COMMAND;
    let featDie = Tor1eDie.TORFeatBaseDie.COMMAND;


    let rollFormula = _buildRollFormula();
    let rollData = {
        actionValue: actionValue,
        difficulty: difficulty,
        modifier: modifier,
        flavor: {
            user: user._id,
            action: actionName,
            owner: {
                id: actor.id,
                img: actor.img,
                name: actor.name
            }
        }
    };

    let rollResult = new Tor1eRoll(rollFormula, rollData).roll();

    let messageData = {
        speaker: ChatMessage.getSpeaker(),

    }

    rollResult.toMessage(messageData);

    return rollResult;
}