import {TORBaseDie, TORFeatBaseDie, TORWearyBaseDie} from "./die.js";
import {tor1eSystemProperties} from "./system-properties.js";

export class Tor1eRoll extends Roll {

    constructor(...args) {
        super(...args);
    }

    /**
     * Return if the Die is a Tor1e Special Die
     * @param die
     * @returns {boolean}
     * @private
     */
    _isCustomDie(die) {
        //TODO change the return with something like
        //let isTor1eDie = (typeof die.getResultValue === "function");
        return die instanceof TORBaseDie
            || die instanceof TORWearyBaseDie
            || die instanceof TORFeatBaseDie
    }

    /**
     * Compute the total amount of the roll for one type of Die
     * If it is a Tor1e special Die, the value is properly compute
     * following the rule of TOR (feat, weary)
     * @param die
     * @param results
     * @returns {string|number|{jsMemoryEstimate: number, jsMemoryRange: [number, number]}|number|PaymentItem|*}
     * @private
     */
    _getValueFrom(die, results) {
        if (this._isCustomDie(die) === true) {
            const cls = die.constructor;
            return cls.getResultValue(results)
        } else {
            return die.total
        }
    }

    /**
     * Compute the total amount for a roll
     * @param dice -> the dice from the roll
     * @param modifier --> may be a modifier to the roll to add
     * @returns {any}
     */
    computeCustomTotal(dice, modifier) {
        return dice
            .flatMap(die => this
                ._getValueFrom(die, die.results))
            .reduce((a, b) => a + b, 0) + modifier
    }

    /**
     * This class is evaluated before the Tor1e system is setup so you can't use CONFIG object to get the value.
     * It's why, you have to use tor1eSystemProperties.path.root instead.
     * @type {string}
     */
    static CHAT_TEMPLATE = `${tor1eSystemProperties.path.root}/templates/sheets/messages/partials/common/roll-tor1e.hbs`;
    static TOOLTIP_TEMPLATE = `${tor1eSystemProperties.path.root}/templates/sheets/messages/partials/common/tooltip-tor1e.hbs`;

    /**
     * Render a Roll instance to HTML
     * This is the fvtt original implementation of the method + add somestuff
     * for the custome Total Amount
     * @param chatOptions {Object}      An object configuring the behavior of the resulting chat message.
     * @return {Promise.<HTMLElement>}  A Promise which resolves to the rendered HTML
     */
    /** @override */
    async render(chatOptions = {}) {
        chatOptions = mergeObject({
            user: game.user._id,
            flavor: this.data.flavor,
            difficulty: this.data.difficulty,
            template: this.constructor.CHAT_TEMPLATE,
            blind: false
        }, chatOptions);
        const isPrivate = chatOptions.isPrivate;

        // Execute the roll, if needed
        if (!this._rolled) this.roll();

        // Define chat data
        this.customTotal = this.computeCustomTotal(this.dice, this.data.modifier);
        if (this.customResult === undefined) {
            this.customResult = this._computeRollResult(this.difficulty, this)
        }
        const chatData = {
            formula: isPrivate ? "???" : this._formula,
            flavor: isPrivate ? null : chatOptions.flavor,
            difficulty: chatOptions.difficulty,
            user: chatOptions.user,
            tooltip: isPrivate ? "" : await this.getTooltip(),
            total: isPrivate ? "?" : Math.round(this.total * 100) / 100,
            customTotal: isPrivate ? "?" : Math.round(this.customTotal * 100) / 100,
            result: this.customResult.result
        };

        // Render the roll display template
        return renderTemplate(chatOptions.template, chatData);
    }

    _computeRollResult(difficulty, roll = {}) {
        if (roll.customTotal >= difficulty) {
            // Roll passes the difficulty
            let nbOfTengwars = this._rollNbOfTengwarRunes()
            if (nbOfTengwars === 0) {
                return {
                    result: {
                        message: CONFIG.tor1e.rollResult.success,
                        cssClass: "success"
                    }
                }
            } else if (nbOfTengwars === 1) {
                return {
                    result: {
                        message: CONFIG.tor1e.rollResult.greatSuccess,
                        cssClass: "success"
                    }
                }
            } else {
                return {
                    result: {
                        message: CONFIG.tor1e.rollResult.extraordinarySuccess,
                        cssClass: "success"
                    }
                }
            }
        } else {
            // Roll doesn't pass the difficulty

            // Roll a failure but can be an automatic success with Gandalf rune
            let gandalf = this._rollAGandalfRune()
            if (gandalf === true) {
                return {
                    result: {
                        message: CONFIG.tor1e.rollResult.automaticSuccess,
                        cssClass: "success"
                    }
                }
            } else {
                return {
                    result: {
                        message: CONFIG.tor1e.rollResult.failure,
                        cssClass: "failure"
                    }
                }
            }
        }
    }

    /**
     * Return if you roll a Rune of Gandalf
     * @returns {boolean}
     * @private
     */
    _rollAGandalfRune() {
        return this._filterSpecificDieAndValue(12, [TORFeatBaseDie]) >= 1;
    }

    /**
     * Return the number of Tengwar runes on Success Dice
     * @returns {*}
     * @private
     */
    _rollNbOfTengwarRunes() {
        return this._filterSpecificDieAndValue(6, [TORWearyBaseDie, TORBaseDie]);
    }

    /**
     * Return The number of specific die that rolled a specific value
     * e.g.: If you roll [6,2,2] for WearyDie, the function with parameter (6,[TorWearyBaseDie]) should return [6]
     * @param valueToFilter -> The value of the Die you want to filter
     * @param dicezz --> The list of Dice classes you want to filter
     * @returns {*}
     * @private
     */
    _filterSpecificDieAndValue(valueToFilter, dicezz) {
        return this.dice
            .filter(die => this._oneOfClasses(dicezz, die))
            .flatMap(r => r.results
                .map(result => result.result))
            .filter(value => value === valueToFilter)
            .length;
    }

    _oneOfClasses(dicezz, die) {
        return dicezz.map(clazz => die instanceof clazz).reduce((a, b) => a || b, false);
    }

    /* -------------------------------------------- */

    /**
     * Render the tooltip HTML for a Roll instance
     * @return {Promise<HTMLElement>}
     */
    /** @override */
    getTooltip() {
        const parts = this.dice.map(die => {
            const cls = die.constructor;
            return {
                formula: die.expression,
                total: this._getValueFrom(die, die.results),
                faces: die.faces,
                flavor: die.flavor,
                rolls: die.results.map(result => {
                    const hasSuccess = result.success !== undefined;
                    const hasFailure = result.failure !== undefined;
                    const isMax = result.result === die.faces;
                    const isMin = result.result === 0;
                    return {
                        result: cls.getResultLabel(result.result),
                        classes: [
                            cls.name.toLowerCase(),
                            "die" + die.faces,
                            result.success ? "success" : null,
                            result.failure ? "failure" : null,
                            result.rerolled ? "rerolled" : null,
                            result.exploded ? "exploded" : null,
                            result.discarded ? "discarded" : null,
                            !(hasSuccess || hasFailure) && isMin ? "min" : null,
                            !(hasSuccess || hasFailure) && isMax ? "max" : null
                        ].filter(c => c).join(" ")
                    }
                })
            };
        });
        return renderTemplate(this.constructor.TOOLTIP_TEMPLATE, {parts});
    }

    /* -------------------------------------------- */
    /** @override */
    toMessage(messageData = {}, {rollMode = null, create = true} = {}) {
        // Perform the roll, if it has not yet been rolled
        if (!this._rolled) this.evaluate();

        const rMode = rollMode || messageData.rollMode || game.settings.get("core", "rollMode");

        let template = CONST.CHAT_MESSAGE_TYPES.ROLL;
        if (["gmroll", "blindroll"].includes(rMode)) {
            messageData.whisper = ChatMessage.getWhisperRecipients("GM");
        }
        if (rMode === "blindroll") messageData.blind = true;
        if (rMode === "selfroll") messageData.whisper = [game.user.id];

        // Prepare chat data
        messageData = mergeObject(
            {
                user: game.user._id,
                type: template,
                content: this.total,
                sound: CONFIG.sounds.dice,
            },
            messageData
        );
        messageData.roll = this;

        // Prepare message options
        const messageOptions = {rollMode: rMode};

        // Either create the message or just return the chat data
        return create ? CONFIG.ChatMessage.entityClass.create(messageData, messageOptions) : messageData;
    }

    /** @override */
    toJSON() {
        const json = super.toJSON();
        json.actionValue = this.data.actionValue;
        json.difficulty = this.data.difficulty;
        json.flavor = this.data.flavor;
        json.data = this.data;
        return json;
    }

    /** @override */
    static fromData(data) {
        const roll = super.fromData(data);
        roll.actionValue = data.actionValue;
        roll.difficulty = data.difficulty;
        roll.flavor = data.flavor;
        roll.data = data.data;
        return roll;
    }

}